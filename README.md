### Alchemy-2 libary
https://alchemy.cs.washington.edu/

https://code.google.com/archive/p/alchemy-2/downloads

##### Install dependencies
sudo apt-get install flex bison

##### Build alchemy-2
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/alchemy-2/alchemy-2.tar.gz \
tar xvzf alchemy-2.tar.gz \
cd ./alchemy-2/src/ \
make depend \
make

executables are located in the sbin folder
https://alchemy.cs.washington.edu/tutorial/tutorial.pdf

##### Weight training a MLN
cd ../..

./alchemy-2/bin/learnwts -i input.mln -t training.db -o output.mln -ne SamePerson

##### Run model inference
./alchemy-2/bin/infer -i output.mln -e evidence.db -r result -q SamePerson

##### View result
cat result

example output: \
SamePerson("1","1") 0.99595 \
SamePerson("1","2") 0.90496 \
SamePerson("2","1") 0.90496 \
SamePerson("2","2") 0.99795
